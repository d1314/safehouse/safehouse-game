extends ESCGame


func _ready() -> void:
	escoria.globals_manager.connect("global_changed", $ESCInventory, "add_new_item_by_id")
	

func element_focused(element_id: String) -> void:
	if escoria.object_manager.get_object(element_id).interactive:
		var focused_object = escoria.object_manager.get_object(element_id)
		
		if "tooltip_name" in focused_object.node:
			$UI/TooltipMargin/Panel.show()
			$UI/TooltipMargin/Panel/CenterContainer/TooltipLabel.text = focused_object.node.tooltip_name


func element_unfocused() -> void:
	$UI/TooltipMargin/Panel.hide()
	$UI/TooltipMargin/Panel/CenterContainer/TooltipLabel.text = ""

func inventory_item_focused(inventory_item_global_id: String) -> void:
	var focused_object = escoria.object_manager.get_object(inventory_item_global_id)
		
	if "tooltip_name" in focused_object.node:
		$UI/TooltipMargin/Panel.show()
		$UI/TooltipMargin/Panel/CenterContainer/TooltipLabel.text = focused_object.node.tooltip_name

func inventory_item_unfocused() -> void:
	self.element_unfocused()


func _on_event_done(_return_code: int, _event_name: String) -> void:
	escoria.action_manager.clear_current_action()
	escoria.action_manager.clear_current_tool()


func left_click_on_item(item_global_id: String, event: InputEvent) -> void:
	escoria.action_manager.set_current_action("look")
	escoria.action_manager.do(
		escoria.action_manager.ACTION.ITEM_LEFT_CLICK, 
		[item_global_id, event], 
		true
	)

func right_click_on_item(item_global_id: String, event: InputEvent) -> void:
	var selected_node = escoria.object_manager.get_object(item_global_id).node
	if "default_action" in selected_node and not selected_node.default_action.empty():
		escoria.action_manager.set_current_action(selected_node.default_action)
	else:
		escoria.action_manager.set_current_action("use")
	escoria.action_manager.do(
		escoria.action_manager.ACTION.ITEM_LEFT_CLICK, 
		[item_global_id, event], 
		true
	)

func hide_main_menu():
	if $UI/main_menu.visible:
		$UI/main_menu.hide()

func show_main_menu():
	if not $UI/main_menu.visible:
		$UI/main_menu.reset()
		$UI/main_menu.show()

func unpause_game():
	if $UI/pause_menu.visible:
		$UI/pause_menu.hide()
		escoria.object_manager.get_object("_camera").node.current = true
		escoria.main.current_scene.game.show_ui()
		escoria.main.current_scene.show()

func pause_game():
	if not $UI/pause_menu.visible:
		$UI/main_menu.reset()
		$UI/pause_menu.set_save_enabled(
			escoria.save_manager.save_enabled
		)
		$UI/pause_menu.show()
		escoria.object_manager.get_object("_camera").node.current = false
		escoria.main.current_scene.game.hide_ui()
		escoria.main.current_scene.hide()

