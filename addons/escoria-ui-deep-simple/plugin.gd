tool
extends EditorPlugin


# Register UI
func _enter_tree():
	call_deferred("_register")


# Deregister UI
func _exit_tree() -> void:
	print("Disabling plugin Escoria UI 9-verbs.")
	EscoriaPlugin.deregister_ui("res://addons/escoria-ui-deep-simple/game.tscn")


# Register UI with Escoria
func _register():
	print("Enabling plugin Escoria UI deep simple.")
	if not EscoriaPlugin.register_ui(self, "res://addons/escoria-ui-deep-simple/game.tscn"):
		get_editor_interface().set_plugin_enabled(
			get_plugin_name(),
			false
		)
