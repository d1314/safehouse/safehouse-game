extends ESCInventoryButton
class_name DeepInventoryButton

func _init(p_item: ESCInventoryItem).(p_item) -> void:
	pass
	

func get_drag_data(position: Vector2):
	var drag_item = self.duplicate()
	set_drag_preview(drag_item)
	return global_id
	
func can_drop_data(position: Vector2, data) -> bool:
	return true
	
func drop_data(position: Vector2, drop_global_id) -> void:
	var our_object = (escoria.object_manager.get_object(global_id) as ESCObject)
	if our_object.events.has("use %s" % drop_global_id):
		var event_to_queue = our_object.events.get("use %s" % drop_global_id)
		escoria.event_manager.queue_event(event_to_queue)
