extends ESCInventoryContainer
class_name DeepInventoryContainer


func add_item(inventory_item: ESCInventoryItem) -> ESCInventoryButton:
	var button = DeepInventoryButton.new(inventory_item)
	add_child(button)
	return button
