# `update_tooltip target`
#
# Updates the tooltip as if the mouse has entered the target
# - target: global id of the target item
#
# @ESC
extends ESCBaseCommand
class_name UpdateTooltipCommand


# Return the descriptor of the arguments of this command
func configure() -> ESCCommandArgumentDescriptor:
	return ESCCommandArgumentDescriptor.new(
		1,
		[TYPE_STRING],
		[null]
	)


# Validate wether the given arguments match the command descriptor
func validate(arguments: Array):
	if not escoria.object_manager.has(arguments[0]):
		escoria.logger.report_errors(
			"update_tooltip: invalid global id",
			[
				"Object with global id %s not found" % [
					arguments[0],
				]
			]
		)
		return false

	return .validate(arguments)


# Run the command
func run(command_params: Array) -> int:
	var item = ((escoria.object_manager.get_object(command_params[0]) as ESCObject).node as ESCItem)
	for child in item.get_children():
		if child is CollisionPolygon2D:
			var mouse_local = item.get_viewport().get_mouse_position()
			var polygon = (child as CollisionPolygon2D).polygon
			
			if Geometry.is_point_in_polygon(mouse_local, polygon):
				item._on_mouse_entered()
				return ESCExecution.RC_OK
	escoria.main.current_scene.game.element_unfocused()
	return ESCExecution.RC_OK
